package com.mycompany.Account.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class MyDatabaseConnection {

	public static Connection myConnection = null;

	public static Connection getMyConnection() {
		if (null != myConnection) {
			return myConnection;
		} else {
			try {
				Class.forName("com.mysql.jdbc.Driver");

				myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return myConnection;
	}
}
