package com.wipro.bank.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mycompany.Account.util.MyDatabaseConnection;

public class NewCustomerServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1496049338487951854L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String customerName = request.getParameter("customerName");
		String balance = request.getParameter("balance");

		try {
			Connection con = MyDatabaseConnection.getMyConnection();

			PreparedStatement ps1 = con.prepareStatement("INSERT INTO `CustomerProfile` (`name`) values(?)");
			ps1.setString(1, customerName);
			Statement stmt = con.createStatement();

			// Add customer details first
			ps1.executeUpdate();

			// retrieve customer ID
			String qryStr = "SELECT * from CustomerProfile where name='" + customerName + "';";

			ResultSet rs = stmt.executeQuery(qryStr);

			int customerId = 0;
			int accountId = 0;

			if (rs.next()) {
				customerId = rs.getInt("id");
			}
			PreparedStatement ps = con.prepareStatement("INSERT INTO `Account` (`customerid`, `Balance`)\n" + 
					" VALUES (?,?)");
			
			ps.setInt(1, customerId);
			ps.setString(2, balance);
			ps.executeUpdate();

			String qryStr1 = "SELECT * from Account where `customerid`='"+customerId+"' and `Balance`='"+balance+"'";

			ResultSet rs1 = stmt.executeQuery(qryStr1);
			if (rs1. next()) {
				accountId = rs1.getInt("Accountid");
			}
			PrintWriter out = response.getWriter();

			out.println("Account details are : ");
			out.println("accountId" + accountId + "\n ---- customerId :" + customerId + "----Balance" + balance);
			RequestDispatcher rd = request.getRequestDispatcher("GeneralResponse.jsp");
			request.setAttribute("output", "<html><body><h3>ACCOUNT CREATED SUCCESSFULLY</h3><h4><u>accountId</u> :- " + accountId + "</h4><h4><u>customerId </u> :- " + customerId + "<br> <h4><u>Balance</u> :- " + balance+"</h4>");
			rd.include(request, response);
			rd.forward(request, response);
		} catch (Exception e2) {
			System.out.println(e2);
		}
		

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("CreateNewAccountForm.jsp");
		rd.forward(request, response);
	}

}
