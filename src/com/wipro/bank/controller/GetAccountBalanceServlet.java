package com.wipro.bank.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wipro.bank.bean.Account;
import com.wipro.bank.service.AccountService;

public class GetAccountBalanceServlet extends HttpServlet {

	private static final long serialVersionUID = -1496049338487951854L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		RequestDispatcher rd = request.getRequestDispatcher("GeneralResponse.jsp");
		String accountId =
				 request.getParameter("accountId");
	
		//CALL ACCOUNT SERVICE
		AccountService process = new AccountService();
		Account acount = process.getBalanceOf(Integer.parseInt(accountId));
		request.setAttribute("output","<html><body><h3>FOUND ACCOUNT</h3><h4><u> ACCOUNT ID </u> :- "+ acount.getAccountID() +
				 "</h4><h4><u>AMOUNT</u> :- " + acount.getBalance()+"</h4>");
		rd.include(request, response);
		rd.forward(request, response);
			
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("getAccountBalance.jsp");
		rd.forward(request, response);
	}

}
