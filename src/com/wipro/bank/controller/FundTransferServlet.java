package com.wipro.bank.controller;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wipro.bank.service.AccountService;

public class FundTransferServlet extends HttpServlet {

	private static final long serialVersionUID = -1496049338487951854L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String fromId = request.getParameter("fromId");
		String toId = request.getParameter("toId");
		String amount = request.getParameter("amount");
		AccountService process = new AccountService();
		RequestDispatcher rd = request.getRequestDispatcher("GeneralResponse.jsp");

		String output =  process.transferFunds(Integer.parseInt(fromId), Integer.parseInt(toId), Float.valueOf(amount));
		
		request.setAttribute("output",output);
		rd.include(request, response);
		rd.forward(request, response);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("TransferFunds.jsp");
		rd.forward(request, response);
	}

}
