package com.wipro.bank.dao;

import java.util.ArrayList;
import java.util.List;

import com.wipro.bank.bean.Account;
import com.wipro.bank.bean.Customer;

public class AccountDao {
	
	public static List<Account> accounts;
	
	public static List<Customer> customers;
	
	String saveAccount(Account account) {
		if(accounts.add(account)) {
			return "success";
		}else {
			return "failure";
		}
	}
	
	public List<Account> findAllAccounts(){
		return accounts;
		
	}
	
	public List<Account> findAllCustomers(){
		return accounts;
		
	}

	public static List<Account> getAccounts() {
		return accounts;
	}

	public static void setAccounts(List<Account> accounts) {
		AccountDao.accounts = accounts;
		
		if(null==customers) {
			customers = new ArrayList<Customer>();
			for(Account account : accounts ) {
				customers.add(account.getCustomer());
			}
		}
		
	}

	public static List<Customer> getCustomers() {
		return customers;
	}

	public static void setCustomers(List<Customer> customers) {
		AccountDao.customers = customers;
	}

}
