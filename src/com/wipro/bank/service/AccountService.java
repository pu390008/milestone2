package com.wipro.bank.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.mycompany.Account.util.MyDatabaseConnection;
import com.wipro.bank.bean.Account;
import com.wipro.bank.bean.Customer;
import com.wipro.bank.dao.AccountDao;

public class AccountService {

	AccountDao accountDao ;
	
	public String addAccount(Account acc) {
		for(Account account : AccountDao.getAccounts() ) {
			if(account.getAccountID()==acc.getAccountID()) {
				return "FAILURE";
			}
		}
		if(AccountDao.getAccounts().add(acc)) {
			return "SUCCESS";
		}
		return "FAILURE";
	}
	
	public List<Account> getAllAccounts(){
		return AccountDao.getAccounts();
	}
	
	public List<Customer> getAllCustomers(){
		return AccountDao.getCustomers();
	}
	
	public String transferFunds(int
			from,int to,double amount) {
		StringBuffer stb = new StringBuffer();
		Connection con = MyDatabaseConnection.getMyConnection();
		String qryStr1 = "SELECT * from Account where `Accountid`='"+from+"' ";
		
		float balanceFromAccount = 0;
		float balanceToAccount = 0;
			try {
				con.setAutoCommit(false);

				Statement stmt = con.createStatement();
				ResultSet rs1 = stmt.executeQuery(qryStr1);
				if (rs1. next()) {
				balanceFromAccount = rs1.getFloat("balance");
				}
				if(balanceFromAccount < amount) {
					stb.append("<html><body><h3>LOW BALANCE CANNOT TRANSFER FUNDS</h3><h4><u>FROM ACCOUNT ID </u> :- " + from + "</h4><h4><u>TO ACCOUNT ID </u> :- " + to + "<br> <h4><u>Balance</u> :- " + amount+"</h4>");

				}else {
					rs1.close();
				      String query = "update Account set balance = ? where Accountid = ?";
				      PreparedStatement preparedStmt = con.prepareStatement(query);
				      preparedStmt.setFloat(1, (float) (balanceFromAccount-amount));
				      preparedStmt.setString(2, ""+from);
				      preparedStmt.executeUpdate();
				      
				      String qryStr2 = "SELECT * from Account where `Accountid`='"+to+"' ";
				      Statement stmt1 = con.createStatement();
						ResultSet rs11 = stmt1.executeQuery(qryStr2);
						if (rs11. next()) {
						balanceToAccount = rs11.getFloat("balance");
						}
						
					//UPDATE THE TO ACCOUNT WITH FUNDS
				      String query1 = "update Account set balance = ? where Accountid = ?";
				      PreparedStatement preparedStmt1 = con.prepareStatement(query1);
				      preparedStmt1.setFloat(1, (float) (balanceToAccount+amount));
				      preparedStmt1.setString(2, ""+to);
				      preparedStmt1.executeUpdate();
				      
				      con.commit();
				      stb.append("<html><body><h3>TRANSFER SUCCESS</h3><h4><u>FROM ACCOUNT ID </u> :- " + from + "</h4><h4><u>TO ACCOUNT ID </u> :- " + to + "<br> <h4><u>AMOUNT</u> :- " + amount+"</h4>");
						
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 try {
					con.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
	//MODULE 1 ASSIGNMENT CODE
		/**Account fromAccount = null ;
		Account toAccount = null;
		for(Account account : AccountDao.getAccounts() ) {
			if(account.getAccountID()==from) {
				fromAccount = account;
			}else if (account.getAccountID()==to) {
				toAccount = account;
			}
		}
		if(null!=fromAccount && null!= toAccount) {
			if(fromAccount.getBalance()<amount) {
				return "INSUFFICIENT FUNDS";
			}else {
				fromAccount.setBalance(fromAccount.getBalance()-amount);
				toAccount.setBalance(toAccount.getBalance()+amount);
				return "SUCCESS";
			}
		}else {
			return "sorry user doesn't exist";
		}**/
			
			 return stb.toString();
	}
	
	public Account getBalanceOf(int
			accountNumber) {
		Account account = new Account();
		Connection con = MyDatabaseConnection.getMyConnection();
		 String qryStr2 = "SELECT * from Account where `Accountid`='"+accountNumber+"' ";
	      Statement stmt1;
	      account.setAccountID(accountNumber);
	      
		try {
			stmt1 = con.createStatement();
			ResultSet rs11 = stmt1.executeQuery(qryStr2);
			if (rs11. next()) {
				account.setBalance(rs11.getFloat("balance"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// ASSIGNMENT MODULE 1 CODE
//		
//		for(Account account : AccountDao.getAccounts() ) {
//			if(account.getAccountID()==accountNumber) {
//				return  account ;
//			}
//		}
		return account;
	}

	
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	
	
}
